<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/store/add', 'StoreController@addStore');
$router->post('/store/update', 'StoreController@updateStoreDetails');
$router->post('/store/update/{store_id}/ownership-proof', 'StoreController@updateStoreStepTwo');
$router->post('/store/update/{store_id}/bank-account', 'StoreController@updateStoreStepThree');
$router->post('/store/update/{store_id}/file/upload', 'StoreFileUploadController@updateStoreStepThreeFileUpload');
$router->post('/store/get/{store_partner_id}', 'StoreController@getStores');
$router->get('/store/details/step1', 'StoreController@getStoreDetailsStep1');
$router->get('/store/details/step2', 'StoreController@getStoreDetailsStep2');

$router->post('/search', 'StoreController@searchStore');
