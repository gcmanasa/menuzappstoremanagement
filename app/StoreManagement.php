<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreManagement extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'store_id',	'store_partner_id',	'store_name', 'store_address', 'type_of_cuisine',	'description','google_business_url','facebook_url','legal_owner_name',	'legal_business_name', 'business_register_number',	'certificate_of_registration',	'bank', 'bank_account_name', 'bank_account_number', 'bsb_number', 'active_flag', 'next_step', 'created_at','updated_at',
    ];
    public $table = 'stores';
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
//        'password',
    ];

    public $timestamps = false;
    protected $primaryKey = 'store_id';
}
