<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use App\StoreManagement;
use DB;

class StoreController extends Controller
{
    use ApiResponser;

    /**
     * Login for a existing user.
     *
     * @param  Request  $request
     * @return Response
     */

    public function __construct()
    {

    }

    public function addStore(Request $request)
    {
        $rules = [
            'store_name' => 'required|string|max:45',
            'store_address' => 'required|string|max:255',
            'type_of_cuisine' => 'required|string|max:45',
            'description' => 'required|string|max:255',
            'google_business_url' => 'required|string|max:255',
            'facebook_url' => 'required|string|max:255',
        ];
        $this->validate($request, $rules);
        DB::beginTransaction();
        $create_store = new StoreManagement([
            'store_partner_id' => $request->store_partner_id,
            'store_name' => $request->store_name,
            'store_address' => $request->store_address,
            'type_of_cuisine' => $request->type_of_cuisine,
            'description' => $request->description,
            'google_business_url' =>$request->google_business_url,
            'facebook_url'=>$request->facebook_url,
            'next_step' => 'ownership-proof',
            'created_at' => Carbon::now()->timestamp,
        ]);
        
       // return $create_store;

        $create_store->save();
        if ($create_store->store_id) {
            DB::commit();
            return $this->successResponse(array('store_id'=> $create_store->store_id, 'next_step'=> 'ownership-proof'), Response::HTTP_OK);
        }else{
            return $this->errorResponse('error_msg','Error while creating store', Response::HTTP_FAILED_DEPENDENCY);
        }
    }
    
    
    

	public function updateStoreDetails(Request $request)
	{
	
		$rules = [
			'store_id' => 'exists:stores,store_id|required',
			'store_partner_id' => 'exists:stores,store_partner_id|required',
		];
		$this->validate($request, $rules);

		DB::beginTransaction();
		$update_store_step1 = StoreManagement::where('store_id', $request->store_id)
			->where('store_partner_id', $request->store_partner_id)
			->update(['store_name' => trim($request->store_name),
				'store_address' => $request->store_address,
				'type_of_cuisine' => $request->type_of_cuisine,
				'description' => $request->description,
				'next_step' => 'ownership-proof',
				'updated_at' => Carbon::now()->timestamp]);
		if ($update_store_step1) {
			DB::commit();
			return $this->successResponse(array('store_id'=>$request->store_id, 'next_step'=>'ownership-proof'), Response::HTTP_OK);
		}else{
			return $this->errorResponse('error_msg','details are not updated', Response::HTTP_BAD_REQUEST);
		}
	}
	
    public function getStoreDetailsStep1(Request $request)
    {
        $rules = [
            'store_id' => 'exists:stores,store_id|required',
            'store_partner_id' => 'exists:stores,store_partner_id|required',
        ];
        $this->validate($request, $rules);
        $get_store_step1_details = StoreManagement::select('store_name', 'store_address', 'type_of_cuisine', 'description')
            ->where('store_id', $request->store_id)
            ->where('store_partner_id', $request->store_partner_id)
            ->get();

        $get_store_step1_details = json_decode($get_store_step1_details,true);

        if(!empty($get_store_step1_details) && is_array($get_store_step1_details) && sizeof($get_store_step1_details) > 0) {
            return $this->successResponse($get_store_step1_details, Response::HTTP_OK);
        }
    }

    public function updateStoreStepTwo(Request $request)
    {
        $rules = [
            'legal_owner_name' => 'required|string|max:45',
            'legal_business_name' => 'required|string|max:45',
            'business_register_number' =>'required|numeric|digits_between:1,10',
            'certificate_of_registration' => 'required|string|max:255',
        ];
        $this->validate($request, $rules);

        $check_store_partner_and_store = StoreManagement::select('store_id', 'store_partner_id', 'active_flag', 'next_step')
            ->where('store_id', $request->store_id)
            ->where('store_partner_id', $request->store_partner_id)
            ->get();

        $check_store_partner_and_store = json_decode($check_store_partner_and_store,true);

        if(!empty($check_store_partner_and_store) && is_array($check_store_partner_and_store) && sizeof($check_store_partner_and_store) > 0) {
            if($check_store_partner_and_store[0]['active_flag'] == 1){
                DB::beginTransaction();
                $update_store_step2 = StoreManagement::where('store_id', $check_store_partner_and_store[0]['store_id'])
                    ->where('store_partner_id', $check_store_partner_and_store[0]['store_partner_id'])
                    ->update(['legal_owner_name' => trim($request->legal_owner_name),
                        'legal_business_name' => $request->legal_business_name,
                        'business_register_number' => $request->business_register_number,
                        'certificate_of_registration' => $request->certificate_of_registration,
                        'next_step' => 'bank-account',
                        'updated_at' => Carbon::now()->timestamp,]);
                if ($update_store_step2) {
                    DB::commit();
                    return $this->successResponse(array('store_id'=> $request->store_id, 'next_step'=> 'bank-account'), Response::HTTP_OK);
                }else{
                    return $this->errorResponse('error_msg','details are not updated', Response::HTTP_BAD_REQUEST  );
                }
            }elseif ($check_store_partner_and_store[0]['active_flag'] == 0){
                DB::beginTransaction();
                $update_store_step2 = StoreManagement::where('store_id', $check_store_partner_and_store[0]['store_id'])
                    ->where('store_partner_id', $check_store_partner_and_store[0]['store_partner_id'])
                    ->update(['legal_owner_name' => trim($request->legal_owner_name),
                        'legal_business_name' => $request->legal_business_name,
                        'business_register_number' => $request->business_register_number,
                        'certificate_of_registration' => $request->certificate_of_registration,
                        'next_step' => 'bank-account',
                        'updated_at' => Carbon::now()->timestamp,]);
                if ($update_store_step2) {
                    DB::commit();
                    return $this->successResponse(array('store_id'=> $request->store_id, 'next_step'=> 'bank-account'), Response::HTTP_OK);
                }else{
                    return $this->errorResponse('error_msg','details are not updated', Response::HTTP_BAD_REQUEST  );
                }
            }else{
                return $this->errorResponse('error_msg','Something went wrong', Response::HTTP_BAD_REQUEST);
            }
        }else{
            return $this->errorResponse('error_msg','store  or store partner id is missing', Response::HTTP_FAILED_DEPENDENCY  );
        }
    }
    
    public function getStoreDetailsStep2(Request $request)
    {
        $rules = [
            'store_id' => 'exists:stores,store_id|required',
            'store_partner_id' => 'exists:stores,store_partner_id|required',
        ];
        $this->validate($request, $rules);
        $get_store_step2_details = StoreManagement::select('legal_owner_name', 'legal_business_name', 'business_register_number', 'certificate_of_registration')
            ->where('store_id', $request->store_id)
            ->where('store_partner_id', $request->store_partner_id)
            ->get();

        $get_store_step2_details = json_decode($get_store_step2_details,true);

        if(!empty($get_store_step2_details) && is_array($get_store_step2_details) && sizeof($get_store_step2_details) > 0) {
            return $this->successResponse($get_store_step2_details, Response::HTTP_OK);
        }
    }

    public function updateStoreStepThree(Request $request)
    {
        $rules = [
            'bank' => 'required|string|max:45',
            'bank_account_name' => 'required|string|max:45',
            'bank_account_number' =>'required|string|max:45',
            'bsb_number' => 'required|string|max:45',

        ];

        $this->validate($request, $rules);

        $check_store_partner_and_store = StoreManagement::where('store_id', $request->store_id)
            ->where('store_partner_id', $request->store_partner_id)
            ->get();

        $check_store_partner_and_store = json_decode($check_store_partner_and_store,true);

        if(!empty($check_store_partner_and_store) && is_array($check_store_partner_and_store) && sizeof($check_store_partner_and_store) > 0) {
            if($check_store_partner_and_store[0]['active_flag'] == 1){
                DB::beginTransaction();
                $update_store_step_three = StoreManagement::where('store_id', $check_store_partner_and_store[0]['store_id'])
                    ->where('store_partner_id', $check_store_partner_and_store[0]['store_partner_id'])
                    ->update(['bank' => trim($request->bank),
                        'bank_account_name' => $request->bank_account_name,
                        'bank_account_number' => $request->bank_account_number,
                        'bsb_number' => $request->bsb_number,
                        'next_step' => '',
                        'updated_at' => Carbon::now()->timestamp,]);
                if ($update_store_step_three) {
                    DB::commit();
                    return $this->successResponse(array('store_id'=> $request->store_id, 'next_step'=> ''), Response::HTTP_OK);
                }else{
                    return $this->errorResponse('error_msg','details are not updated', Response::HTTP_BAD_REQUEST  );
                }
            }elseif ($check_store_partner_and_store[0]['active_flag'] == 0){
                DB::beginTransaction();
                $update_store_step_three = StoreManagement::where('store_id', $check_store_partner_and_store[0]['store_id'])
                    ->where('store_partner_id', $check_store_partner_and_store[0]['store_partner_id'])
                    ->update(['bank' => trim($request->bank),
                        'bank_account_name' => $request->bank_account_name,
                        'bank_account_number' => $request->bank_account_number,
                        'bsb_number' => $request->bsb_number,
                        'next_step' => '',
                        'updated_at' => Carbon::now()->timestamp]);
                if ($update_store_step_three) {
                    DB::commit();
                    return $this->successResponse(array('store_id'=> $request->store_id, 'next_step'=> ''), Response::HTTP_OK);
                }else{
                    return $this->errorResponse('error_msg','details are not updated', Response::HTTP_BAD_REQUEST  );
                }
            }else{
                return $this->errorResponse('error_msg','Something went wrong', Response::HTTP_BAD_REQUEST  );
            }

        }else{
            return $this->errorResponse('error_msg','store id or store partner is missing', Response::HTTP_FAILED_DEPENDENCY  );
        }
    }

    public function getStores($store_partner_id)
    {

        $get_stores = StoreManagement::select('*')
            ->where('store_partner_id', $store_partner_id)
//            ->groupBy('store_id')
            ->get();

        $get_stores = json_decode($get_stores,true);
        if(!empty($get_stores) && is_array($get_stores) && sizeof($get_stores) > 0){
            return $this->successResponse($get_stores, Response::HTTP_OK);
        }else{
            return $this->errorResponse('error_msg','No data found', Response::HTTP_BAD_REQUEST  );
        }
    }



    public function updateStoreStepThreeFileUpload(Request $request) {

//            $rules = [
//                'profile_image' => 'required|certificate_of_registration|max:2048',
//            ];
//
//            $this->validate($request, $rules);
        if ($files = $request->file('profile_image')) {
            // Define upload path

            $destinationPath = './store/step3/dcuments/';
            // Upload Orginal Document
            $document_upload = date('YmdHis') . "." . $request->store_id . "." . $files->getClientOriginalExtension();
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0777, true);
            }
            $files->move($destinationPath, $document_upload);

            $insert['upload_path'] = "$destinationPath/$document_upload";
            $upload_document_path= StoreManagement::where('store_id', $request->store_id)
                ->where('store_partner_id', $request->store_partner_id)
                ->get();

            $upload_document_path = json_decode($upload_document_path,true);

            var_dump($upload_document_path);die();
            if(!empty($image_result) && is_array($image_result) && sizeof($image_result) > 0){
                $image_update = DB::table('user_profile_details')->where('user_id',Auth::user()->id)->update([ 'image_path' => $insert['image_path']]);
                if($image_update){
                    $image= DB::table('user_profile_details')
                        ->select('image_path')
                        ->where('user_id',Auth::user()->id)->get();
                    $image = json_decode($image,true);
                    if(!empty($image) && sizeof($image) > 0){
                        if(!empty($image[0]['image_path'])){
                            return $this->successResponse($image[0]['image_path'], Response::HTTP_OK);

                        }
                    }
                }
            }
        }
    }
    
    
    
    
    public function searchStore(Request $request)
{
       
    $data = $request->get('data');
        if(!empty($data)){
            //return "yess";
          $store_info = DB::table('stores')
                      ->select('store_name')
                      ->where('store_name', 'like', "%{$data}%")
                      ->get();
          return Response()->json([
              'data' => $store_info
          ], 200);
        }else{
            return Response()->json([
             // 'message' => 'not-found',
              'data' => $store_info
          ], 200);
        }
    
}
}
