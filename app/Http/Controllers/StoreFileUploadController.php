<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use App\StoreManagement;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StoreFileUploadController extends Controller
{
    use ApiResponser;

    public function __construct()
    {

    }
    public function updateStoreStepThreeFileUpload(Request $request) {



        $files = $request->profile_image;
        if ($files) {
;

            $picName = $files['profile_image']['name'];
            $picName = uniqid() . '_' . $picName;
            $path = 'uploads';

            $destinationPath = './store/step3/dcuments/';
            $request->profile_image('image')->move($destinationPath, $picName);


die();
            // Define upload path

            $destinationPath = './store/step3/dcuments/';
            // Upload Orginal Document
            $document_upload = date('YmdHis') . "." . $request->store_id . "." . $files['profile_image']['name']->getClientOriginalExtension();
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0777, true);
            }
            $files['profile_image']['name']->move($destinationPath, $document_upload);

            $insert['upload_path'] = "$destinationPath/$document_upload";


            $upload_document_path= StoreManagement::where('store_id', $request->store_id)
                ->where('store_partner_id', $request->store_partner_id)
                ->get();

            $upload_document_path = json_decode($upload_document_path,true);


            if(!empty($image_result) && is_array($image_result) && sizeof($image_result) > 0){
                $image_update = DB::table('user_profile_details')->where('user_id',Auth::user()->id)->update([ 'image_path' => $insert['image_path']]);
                if($image_update){
                    $image= DB::table('user_profile_details')
                        ->select('image_path')
                        ->where('user_id',Auth::user()->id)->get();
                    $image = json_decode($image,true);
                    if(!empty($image) && sizeof($image) > 0){
                        if(!empty($image[0]['image_path'])){
                            return $this->successResponse($image[0]['image_path'], Response::HTTP_OK);

                        }
                    }
                }
            }
        }
    }
}
