<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class AuthenticateAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
        $validSecrets = explode(',', env('ACCEPTED_SECRETS'));
        if(in_array($request->header('Authorization'), $validSecrets))
        {
            return $next($request);
        }
        return \response()->json(['error' => "Unauthorized", 'code' => 401], 401);
        //abort(Response::HTTP_UNAUTHORIZED);
    }
}