<?php

namespace App\Traits;

use GuzzleHttp\Client;

trait ConsumeExternalFileService
{
    /**
     * Send request to any service
     * @param $method
     * @param $requestUrl
     * @param array $formParams
     * @param array $headers
     * @return string
     */
    public function performFileRequest($method, $requestUrl, $formData = [], $headers = [])
    {
        $client = new Client([
            'base_uri'  =>  $this->baseUri,
        ]);
        
        if(isset($this->secret))
        {
            $headers['Authorization'] = $this->secret;
        }
        
        $response = $client->request($method, $requestUrl, [
            'headers'     => $headers,
            'multipart' => $formData
        ]);
        
        return $response->getBody()->getContents();
    }
}